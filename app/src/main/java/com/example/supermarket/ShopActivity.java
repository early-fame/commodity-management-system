package com.example.supermarket;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.supermarket.Tools.DataDB;
import com.example.supermarket.Tools.DataTools;
import com.example.supermarket.Tools.ShopAdapter;
import com.example.supermarket.Tools.Tools;

import java.util.ArrayList;
import java.util.List;

public class ShopActivity extends AppCompatActivity {

    public static List<Tools> list = new ArrayList<>();
    public static ListView listView;
    public static ShopAdapter adapter;

    Button button;

    DataTools d_tools;
    DataDB db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shop);
        init();
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int i = 0;
                int sum = 0;
                while (i<list.size()){
                    int price = Integer.parseInt(list.get(i).price);
                    int count = Integer.parseInt(list.get(i).count);
                    sum = sum + price*count;
                    i++;
                }
                Toast.makeText(ShopActivity.this,"总金额为："+sum,Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void init(){
        db = new DataDB(ShopActivity.this,"Data.db",null,1);
        d_tools = new DataTools(db);
        list = d_tools.select(LoginActivity.account);
        listView = findViewById(R.id.list);
        adapter = new ShopAdapter(ShopActivity.this,R.layout.item_shop,list);
        listView.setAdapter(adapter);
        button = findViewById(R.id.set);
    }
}