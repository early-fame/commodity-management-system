package com.example.supermarket.Tools;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.supermarket.LoginActivity;
import com.example.supermarket.R;
import com.example.supermarket.ShopActivity;

import java.util.ArrayList;
import java.util.List;

public class ShopAdapter extends ArrayAdapter<Tools> {

    Context context;
    DataDB db;
    DataTools d_tools;
    List<Tools> list = new ArrayList<>();

    public ShopAdapter(@NonNull Context context, int resource, List<Tools> data) {
        super(context, resource,data);
        this.context = context;
        this.context = context;
        db = new DataDB(context,"Data.db",null,1);
        d_tools = new DataTools(db);
        this.list = data;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view;
        final ViewHolder holder;
        if (convertView == null){
            view = LayoutInflater.from(context).inflate(R.layout.item_shop,parent,false);
            TextView name = view.findViewById(R.id.name);
            TextView count = view.findViewById(R.id.count);
            ImageView image = view.findViewById(R.id.image);
            Button add = view.findViewById(R.id.add);
            Button remove = view.findViewById(R.id.reduce);
            TextView price = view.findViewById(R.id.price);
            holder = new ViewHolder(name,count,image,add,remove,price);
            view.setTag(holder);
        }else {
            view = convertView;
            holder = (ViewHolder)view.getTag();
        }
        Tools tools = getItem(position);
        if (tools != null){
            holder.name.setText(tools.name);
            holder.count.setText(tools.count);
            holder.image.setImageResource(tools.image);
            holder.price.setText(tools.price);
        }
        holder.add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String count = d_tools.select(LoginActivity.account,list.get(position).name,"count");
                int i = 0;
                if (count != null&&count.equals("")){
                    i = 1;
                }
                if (count != null&&!count.equals("")){
                    i = Integer.parseInt(count);
                    i++;
                    ShopActivity.list = d_tools.select(LoginActivity.account);
                    ShopActivity.adapter = new ShopAdapter(context,R.layout.item_shop,ShopActivity.list);
                    ShopActivity.listView.setAdapter(ShopActivity.adapter);
                    d_tools.update(LoginActivity.account,list.get(position).name,""+i,"count");
                    holder.count.setText(""+i);
                }
            }
        });
        holder.remove.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String count = d_tools.select(LoginActivity.account,list.get(position).name,"count");
                int i = 0;
                if (count != null&&count.equals("")){
                    i = 1;
                }
                if (count != null&&!count.equals("")){
                    i = Integer.parseInt(count);
                    if (i == 1){
                        d_tools.delete(LoginActivity.account,list.get(position).name);
                        ShopActivity.list = d_tools.select(LoginActivity.account);
                        ShopActivity.adapter = new ShopAdapter(context,R.layout.item_shop,ShopActivity.list);
                        ShopActivity.listView.setAdapter(ShopActivity.adapter);
                        return;
                    }
                    ShopActivity.list = d_tools.select(LoginActivity.account);
                    ShopActivity.adapter = new ShopAdapter(context,R.layout.item_shop,ShopActivity.list);
                    ShopActivity.listView.setAdapter(ShopActivity.adapter);
                    i--;
                    d_tools.update(LoginActivity.account,list.get(position).name,""+i,"count");
                    holder.count.setText(""+i);
                }
            }
        });
        return view;
    }


    public class ViewHolder{

        public TextView name;
        public TextView count;
        public ImageView image;
        public TextView price;
        public Button add;
        public Button remove;

        public ViewHolder(TextView name, TextView count, ImageView image, Button add,Button remove,TextView price){
            this.count = count;
            this.image = image;
            this.name = name;
            this.add = add;
            this.remove = remove;
            this.price = price;
        }
    }
}
