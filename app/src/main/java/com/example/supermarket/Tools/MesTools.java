package com.example.supermarket.Tools;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class MesTools {

    SQLiteDatabase db;

    public MesTools(MesDB db){
        this.db = db.getWritableDatabase();
    }

    //插入
    public void insert(String account,String mes){
        ContentValues values = new ContentValues();
        values.put("account",account);
        values.put("mes",mes);
        db.insert("Mes",null,values);
    }

    //查询
    public List<String> select(String account){
        List<String> list = new ArrayList<>();
        Cursor cursor = db.query("Mes",null,null,null,null,null,null);
        if (cursor.moveToFirst()){
            do {
                String mes = cursor.getString(cursor.getColumnIndex("mes"));
                String s_account = cursor.getString(cursor.getColumnIndex("account"));
                if (account != null){
                    if (account.equals(s_account)){
                        list.add(mes);
                    }
                }
            }while (cursor.moveToNext());
        }
        return list;
    }

    public void delete(String account,String mes){
        String[] s = {account,mes};
        db.delete("Mes","account = ? AND mes = ?",s);
    }
}
