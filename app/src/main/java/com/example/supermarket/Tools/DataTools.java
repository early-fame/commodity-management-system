package com.example.supermarket.Tools;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

public class DataTools {

    SQLiteDatabase db;

    public DataTools(DataDB db){
        this.db = db.getWritableDatabase();
    }

    //插入
    public void insert(String account,String name,String price,String count,int image){
        ContentValues values = new ContentValues();
        values.put("account",account);
        values.put("name",name);
        values.put("price",price);
        values.put("count",count);
        values.put("image",image);
        db.insert("Data",null,values);
    }

    //查询
    public List<Tools> select(String account){
        List<Tools> list = new ArrayList<>();
        Cursor cursor = db.query("Data",null,null,null,null,null,null);
        if (cursor.moveToFirst()){
            do {
                String name = cursor.getString(cursor.getColumnIndex("name"));
                String price = cursor.getString(cursor.getColumnIndex("price"));
                String count = cursor.getString(cursor.getColumnIndex("count"));
                String s_account = cursor.getString(cursor.getColumnIndex("account"));
                int image = cursor.getInt(cursor.getColumnIndex("image"));
                if (account != null){
                    if (account.equals(s_account)){
                        list.add(new Tools(name,count,image,price));
                    }
                }
            }while (cursor.moveToNext());
        }
        return list;
    }

    public boolean select(String account,String name){
        boolean b = false;
        Cursor cursor = db.query("Data",null,null,null,null,null,null);
        if (cursor.moveToFirst()){
            do {
                b = false;
                String s_name = cursor.getString(cursor.getColumnIndex("name"));
                String s_account = cursor.getString(cursor.getColumnIndex("account"));
                if (s_account != null&&s_name != null&&account != null && name != null){
                    if (s_account.equals(account)&&s_name.equals(name)){
                        b = true;
                        break;
                    }
                }
            }while (cursor.moveToNext());
        }
        return b;
    }

    public String select(String account,String name,String s){
        String result = "";
        Cursor cursor = db.query("Data",null,null,null,null,null,null);
        if (cursor.moveToFirst()){
            do {
                result = "";
                String s_name = cursor.getString(cursor.getColumnIndex("name"));
                String s_account = cursor.getString(cursor.getColumnIndex("account"));;
                String s1 = cursor.getString(cursor.getColumnIndex(s));
                if (account.equals(s_account)&&s_name.equals(name)){
                    result = s1;
                    break;
                }
            }while (cursor.moveToNext());
        }
        return result;
    }

    public void update(String account,String name,String count,String key){
        String[] s = {account,name};
        ContentValues values = new ContentValues();
        values.put(key,count);
        db.update("Data",values,"account = ? AND name = ?",s);
    }

    public void delete(String account,String name){
        String[] s = {account,name};
        db.delete("Data","account = ? AND name = ?",s);
    }
}
