package com.example.supermarket.Tools;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.supermarket.LoginActivity;
import com.example.supermarket.R;
import com.example.supermarket.ShopActivity;

import java.util.ArrayList;
import java.util.List;

public class MesAdapter extends ArrayAdapter<String> {

    Context context;
    List<String> list = new ArrayList<>();

    public MesAdapter(@NonNull Context context, int resource, List<String> data) {
        super(context, resource,data);
        this.context = context;
        this.context = context;
        this.list = data;
    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view;
        final ViewHolder holder;
        if (convertView == null){
            view = LayoutInflater.from(context).inflate(R.layout.item_mes,parent,false);
            TextView mes = view.findViewById(R.id.tv_mes);
            holder = new ViewHolder(mes);
            view.setTag(holder);
        }else {
            view = convertView;
            holder = (ViewHolder)view.getTag();
        }
        holder.mes.setText(list.get(position));
        return view;
    }


    public class ViewHolder{

        public TextView mes;

        public ViewHolder(TextView mes){
            this.mes = mes;
        }
    }
}
