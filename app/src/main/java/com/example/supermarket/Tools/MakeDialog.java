package com.example.supermarket.Tools;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.supermarket.LoginActivity;
import com.example.supermarket.R;

public class MakeDialog extends Dialog {

    View view;
    Context context;

    private MesDB db;
    private MesTools tools;
    private Button ok,cancel;
    private EditText ed_mes;
    public MakeDialog(@NonNull Context context) {
        super(context);
        this.context = context;
        view = LayoutInflater.from(context).inflate(R.layout.dialog,null,false);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(view);
        db = new MesDB(context,"Mes.db",null,1);
        tools = new MesTools(db);
        ok = findViewById(R.id.ok);
        cancel = findViewById(R.id.cancel);
        ed_mes = findViewById(R.id.ed_mes);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!ed_mes.getText().toString().equals("")){
                    tools.insert(LoginActivity.account,ed_mes.getText().toString());
                    ed_mes.setText("");
                    MesActivity.myShow(context);
                    dismiss();
                }else {
                    Toast.makeText(context,"输入不能为空",Toast.LENGTH_SHORT).show();
                }
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });
    }
}
