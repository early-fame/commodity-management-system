package com.example.supermarket.Tools;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.example.supermarket.LoginActivity;
import com.example.supermarket.R;

import java.util.ArrayList;
import java.util.List;

public class MesActivity extends AppCompatActivity {

    public static ListView listView;
    public static List<String> list = new ArrayList<>();
    public static MesAdapter adapter;
    public static MesDB db;
    public static MesTools tools;
    private Button add;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mes);
        init();
        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MakeDialog dialog = new MakeDialog(MesActivity.this);
                dialog.show();
            }
        });
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, final int i1, long l) {
                AlertDialog dialog = new AlertDialog.Builder(MesActivity.this)
                        .setTitle("删除")
                        .setMessage("是否删除此留言")
                        .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        })
                        .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                tools.delete(LoginActivity.account,list.get(i1));
                                myShow(MesActivity.this);
                            }
                        }).show();
                return true;
            }
        });
    }

    private void init(){
        db = new MesDB(MesActivity.this,"Mes.db",null,1);
        tools = new MesTools(db);
        list = tools.select(LoginActivity.account);
        listView = findViewById(R.id.list);
        adapter = new MesAdapter(MesActivity.this,R.layout.item_mes,list);
        listView.setAdapter(adapter);
        add = findViewById(R.id.add);
    }

    public static void myShow(Context context){
        list = tools.select(LoginActivity.account);
        adapter = new MesAdapter(context,R.layout.item_mes,list);
        listView.setAdapter(adapter);
    }
}